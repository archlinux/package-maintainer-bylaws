all: package-maintainer-bylaws.html

%.html: %.adoc
	asciidoc -a toc $<

clean:
	rm package-maintainer-bylaws.html

.PHONY: all clean
